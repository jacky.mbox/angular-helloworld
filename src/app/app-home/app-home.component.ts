import { Component } from '@angular/core';

@Component({
  selector: 'app-home',
  template: `
    <p>Hello World!</p>
  `
})
export class AppHomeComponent {

  constructor() { }

}
