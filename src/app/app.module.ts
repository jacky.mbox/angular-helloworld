import { NgModule } from '@angular/core';
import { BrowserModule } from "@angular/platform-browser";
import { CommonModule } from '@angular/common';
import { AppHomeComponent } from './app-home/app-home.component';

@NgModule({
  declarations: [
    AppHomeComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
  ],
  bootstrap: [AppHomeComponent]
})
export class AppModule { }
